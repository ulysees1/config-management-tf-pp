# Configuration Management using Terraform and Puppet

## Prerequisites
- Puppet agent must be installed according to the operating system
- In my case, I used Puppet agent for windows
- Install Terraform and set its exe path in PATH environment variable
- Go to AWS console and select EC2 dashboard
- In that select Microsoft Windows instance type
- Note the ami for the particular instance

## Terraform Configuration
- Create a main.tf file
- Give provider as "aws" and choose desiered region
- In resource, provide instance name, and provide the ami id of the instance and provide desiered instance type
- Add a provisioner for files and provide path for puppet manifests. This is where the manifest files will be saved and provide a destination folder.
- Add a provisioner for executing the puppet manifest

## Puppet manifest
- Create a site.pp file
- Add a node and choose provide as Windows
- This Puppet manifest installs 7-Zip and ensures the Windows Update service is running and enabled

## Integration
- Now initiate the terraform using **terraform init** command in the terminal
- Use **terraform validate** for validation
- Use **terraform plan** to see all the plans available for the terraform file
- Use **terraform apply**. This will initiate the building of aws instance
- If you go the instances in aws console you can see that the instance is up and running
